const fetch = require('node-fetch');
const {map} = require('ramda');
const {isArray} = require('ramda-adjunct');

module.exports.getResponseHeaders = link => {
    return fetch(link).then(res => {
        // dirty type cast to object
        return map( v => isArray(v) ? v.join(';') : v, res.headers.raw());
    });
};