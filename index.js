const fs = require('fs');
const Promise = require('bluebird');
const {filter, findIndex, map, toPairs} = require('ramda');
const getIP = Promise.promisify(require('external-ip')());
const Mustache = require('mustache');

const {getResponseHeaders} = require('./helpers');

// perform {attempt} calls in total
let attempts = 20;

// perform attempts on these links
const links = [
    'https://media.smooch.io/conversations/9c8b9036040215917aa10924/eBahBOFpq267H4ZaTMkFo4ki.zip',
    'https://media.smooch.io/conversations/9c8b9036040215917aa10924/zagzAkUyuBbDY3sC0fomqzy9.jpg'
];

const linkReportTemplate = fs.readFileSync(__dirname + '/templates/linkReport.mustache', 'utf8');
const reportTemplate = fs.readFileSync(__dirname + '/templates/report.mustache', 'utf8');

let report = {
    date: (new Date()).toISOString(),
    ip: '1.2.3.4',
    links: [],
    reports: []
};

report.links = links.map(name => {return {name, attempts, successRate: '0'};});

// all the requests
let promises = [];

// quit after 60 seconds anyways
const delay = new Promise(res => {
    setTimeout(res, 60000);
});

for (let linkIndex = 0; linkIndex < links.length; linkIndex++) {
    let link = links[linkIndex];
    let linkAttempts = [];
    for (let attempt = 0; attempt < attempts; attempt++) {
        linkAttempts.push(getResponseHeaders(link).then(headers => {
            report.reports.push({
                link,
                attempt: attempt + 1,
                attempts,
                headers: map(el => {return {name: el[0], value: el[1]};}, toPairs(headers))
            });
            return headers;
        }));
    }
    promises.push(Promise.all(linkAttempts).then(res => {
        report.links[findIndex(e => e.name === link, report.links)].successRate = filter(headers => !!headers['access-control-allow-origin'], res).length / attempts * 100;
    }));
}

getIP().then(ip => {
    report.ip = ip;
    console.log(`Fetching response headers of ${links.length} links, ${attempts} attempts each`);
    return Promise.race([
        Promise.all(promises),
        delay
    ]);
}).then(() => {
    report.reports = report.reports.map(report => {
        return {content: Mustache.render(linkReportTemplate, report)};
    });
    fs.writeFileSync(__dirname + `/reports/${(new Date()).toISOString()}.log`, Mustache.render(reportTemplate, report));
    process.exit();
});

